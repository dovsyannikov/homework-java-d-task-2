package com.company;

import java.util.Locale;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Task2dot1();
        Task2dot2();
    }
    public static void Task2dot1() {
        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
        //из заранее известных (значения задаются в переменных/константах/моделях) курсов (например,
        //курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.

        System.out.println();
        System.out.println("Task 2.1");

        //init variables

        String USD = "USD";

        Banks[] bank = new Banks[3];
        bank[0] = new Banks();
        bank[0].usdCourse = 24.5f;
        bank[0].bankName = "privatbank";

        bank[1] = new Banks();
        bank[1].usdCourse = 30.5f;
        bank[1].bankName = "pumb";

        bank[2] = new Banks();
        bank[2].usdCourse = 44.5f;
        bank[2].bankName = "oschadbank";

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change to USD: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter bank
        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
        String bankName = scan.nextLine();

        for (int i = 0; i < bank.length; i++) {
            if (bankName.equals(bank[i].bankName)) {
                System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / bank[i].usdCourse));
                break;
            } else if (i == bank.length - 1) {
                System.out.println("Smth went wrong");
            }
        }
    }

    public static void Task2dot2() {
        //2. Дана программа для конвертации валют по введенному курсу (JavaExampleI.java).
        //2.1. На ее основе необходимо сделать программу для конвертации гривны в доллар по одному
        //из заранее известных (значения задаются в переменных/константах/моделях) курсов
        //(например, курс ПриватБанка, ОщадБанка, и банка ПУМБ). Название банка вводится с консоли.
        System.out.println();
        System.out.println("Task 2.2");

        //init variables

        String USD = "USD";
        String EUR = "EUR";
        String RUB = "RUB";

        Banks[] bank = new Banks[3];
        bank[0] = new Banks();
        bank[0].usdCourse = 24.5f;
        bank[0].bankName = "privatbank";
        bank[0].eurCourse = 30.01f;
        bank[0].rubCourse = 0.41f;

        bank[1] = new Banks();
        bank[1].usdCourse = 30.5f;
        bank[1].bankName = "pumb";
        bank[1].rubCourse = 0.42f;
        bank[1].eurCourse = 31f;

        bank[2] = new Banks();
        bank[2].usdCourse = 44.5f;
        bank[2].bankName = "oschadbank";
        bank[2].eurCourse = 30f;
        bank[2].rubCourse = 0.43f;

        //init scanner
        Scanner scan = new Scanner(System.in);

        //enter amount of money
        System.out.println("Enter the amount of money that you want to change: ");
        int amount = Integer.parseInt(scan.nextLine());

        //enter currency
        System.out.println("Enter to what kind of currency do you want to change your grivnas (you can use only EUR or USD or RUB): ");
        String currency = scan.nextLine();

        //enter bank
        System.out.println("Enter from which bank do you want to convert your grivnas (you can use only PUMB or PRIVATBANK or OSHADBANK): ");
        String chosenBank = scan.nextLine();

        //convert uah to preferred currency
        for (int i = 0; i < 3; i++) {
            if (chosenBank.equalsIgnoreCase(bank[i].bankName)) {
                if (currency.equalsIgnoreCase(EUR)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", EUR, amount / bank[i].eurCourse));
                } else if (currency.equalsIgnoreCase(USD)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", USD, amount / bank[i].usdCourse));
                } else if (currency.equalsIgnoreCase(RUB)) {
                    System.out.println(String.format(Locale.US, "You money in %s: %.2f", RUB, amount / bank[i].rubCourse));
                } else {
                    System.err.println("Sorry for inconvenience. Please try again later :)");
                }
            }
        }
    }
}

